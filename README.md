# laravel-oauth

## Installation

Add to your composer file 

```json
"repositories": [
    {
        "name": "glt/laravel-oauth",
        "url": "https://gitlab.com/MykolaRohoza/laravel-oauth.git",
        "type": "git"
    }
]
```
Run in command line
```bach
composer require glt/laravel-oauth --prefer-source
```
Open config/app.php and add service provider
```php 
'providers' => [

    // Other Service Providers

    GLT\LaravelOauth\AuthorizedSendServiceProvider::class,
],
```

Open .env and add authentication data
```yaml
AUTH_EXT_URL=https://example.com
AUTH_EXT_TOKEN=oauth/token
AUTH_EXT_USERNAME=admin@admin.com
AUTH_EXT_PASSWORD=12345678
AUTH_EXT_CLIENT_ID=client_id
AUTH_EXT_CLIENT_SECRET=client_secret
```

Create "auth" directory in root of your project