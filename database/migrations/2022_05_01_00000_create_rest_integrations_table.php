<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rest_integrations', function (Blueprint $table) {
            $table->id();

            $table->string('slug')
                ->index()
                ->unique();
            $table->string('description')
                ->nullable()
                ->default(null);

            $table->string('base_url');
            $table->string('token_url');
            $table->string('username');
            $table->string('password');

            $table->string('client_id');
            $table->string('client_secret');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rest_integrations');
    }
}
