<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOauthCachesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth_caches', function (Blueprint $table) {

			$table->foreignId('user_id')
                ->primary()
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->string('token_type')->nullable();
			$table->integer('expires_in')->nullable();
			$table->text('access_token')->nullable();
			$table->text('refresh_token')->nullable();
			$table->integer('expires_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth_caches');
    }
}
