<?php
return [
    'base_url' => env('AUTH_EXT_URL', null),
    'token_url' => env('AUTH_EXT_TOKEN', null),
    'username' => env('AUTH_EXT_USERNAME', null),
    'password' => env('AUTH_EXT_PASSWORD', null),
    'client_secret' => env('AUTH_EXT_CLIENT_SECRET', null),
    'client_id' => env('AUTH_EXT_CLIENT_ID', null),
    'cache_file' => base_path('auth/auth.cache.json'),



    'driver' => env('AUTH_EXT_DRIVER', 'json'),
    'token_insurance' => env('AUTH_EXT_TOKEN_INSURANCE', 35),
];
