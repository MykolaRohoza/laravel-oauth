<?php

namespace GLT\LaravelOauth;

use Illuminate\Support\ServiceProvider;


class AuthorizedSendServiceProvider extends ServiceProvider
{
	public function boot()
	{
		if ($this->app->runningInConsole()) {
			$this->registerMigrations();
		}
	}

    /**
     * Register the application services.
     */
    public function register()
    {

        $this->registerConfigs();

        if ($this->app->runningInConsole()) {
            $this->registerPublishableResources();
            //$this->registerConsoleCommands();
        }

    }


    /**
     * Register the publishable files.
     */
    private function registerPublishableResources()
    {
        $publishablePath = dirname(__DIR__).'/publishable';

        $publishable = [
            'config' => [
                "{$publishablePath}/config/glt_auth.php" => config_path('glt_auth.php'),
            ],

        ];
        foreach ($publishable as $group => $paths) {
            $this->publishes($paths, $group);
        }
    }

    public function registerConfigs()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__).'/publishable/config/glt_auth.php',
            'glt_auth'
        );
    }


    /**
     * Register the commands accessible from the Console.
     */
    private function registerConsoleCommands()
    {
        $this->commands(Commands\InstallCommand::class);
        $this->commands(Commands\ControllersCommand::class);
        $this->commands(Commands\AdminCommand::class);
    }

	protected function registerMigrations()
	{
		$this->loadMigrationsFrom(__DIR__.'/../database/migrations');
	}

}
