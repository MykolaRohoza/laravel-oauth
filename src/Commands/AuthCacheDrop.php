<?php

namespace GLT\LaravelOauth\Commands;

use App\Models\User;
use GLT\LaravelOauth\Drivers\AuthCache;
use GLT\LaravelOauth\Models\OauthCache;
use Illuminate\Console\Command;

class AuthCacheDrop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'glt-oauth:cache-drop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

		if(AuthCache::config('driver') === 'database'){
			$user = $this->getUser();
			if($user){
				$count = OauthCache::query()
					->where('user_id', '=', $user->id)
					->delete();
				$this->info("$count Oauth Caches were delete for `{$user->name}`");
			}
			$this->info("You have not active oauth caches");
		} else {
			$this->dropJsonCache();
		}


	}

	protected function dropJsonCache()
	{
		$filename = AuthCache::config('cache_file');
		unset($filename);
	}

	protected function getUser(): ?User
	{
		$users = $this->getUsersForSelection();
		if($users){
			$selection = $this->choice("Select user to clear cache", array_keys($users));
			return $this->getUserFromSelection($selection, $users);
		}
		return null;
	}


	protected function getUsersForSelection(): array
	{
		$users = [];
		$_users = User::whereIn('id', OauthCache::all()->pluck('user_id')->toArray())->get();
		foreach ($_users as $_user){
			$show_name = $_user->name . ' - ' . preg_replace('~(^.+\@.{1}).+?(.{1}\..+)~', '$1...$2', $_user->email);
			$users[$show_name] = $_user->id;
		}
		return $users;
	}

	protected function getUserFromSelection(string $selection, array $users): User
	{
		$id = $users[$selection];
		return User::find($id);
	}

}