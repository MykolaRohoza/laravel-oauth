<?php

namespace GLT\LaravelOauth\Drivers;
use GLT\LaravelOauth\Services\AuthorizedSendService;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Client\Response;

use ErrorException;
use DomainException;

/**
 * @property-read string $token_type
 * @property-read int $expires_in
 * @property-read string $access_token
 * @property-read string $refresh_token
 * @property-read int $expires_at
 *
 *
 * @property-read string $base_url
 * @property-read string $token_url
 *
 * @property-read string $client_id
 * @property-read string $client_secret
 * @property-read string $username
 * @property-read string $password
 */

abstract class AuthCache implements iAuthCache
{
    protected array $token_fields = [
        'token_type' => null,
        'expires_in' => null,
        'access_token' => null,
        'refresh_token' => null,
        'expires_at' => 0,
    ];

    protected array $access_fields = [
        'base_url' => null,
        'token_url' => null,
        'client_id' => null,
        'client_secret' => null,
        'username' => null,
        'password' => null,
    ];


	protected AuthorizedSendService $send_service;
	protected ?User $user;

	public function __construct(AuthorizedSendService $send_service, ?User $user = null)
	{
		$this->send_service = $send_service;
        $this->user = $user;
		$this->init();
	}

	/**
	 * @return string|null
	 */
	public function getAuthorization(): string
	{
		return "$this->token_type $this->access_token";
	}


    /**
     * @throws DomainException
     */
    public function __get(string $name)
    {
        if(!array_key_exists($name, array_merge($this->access_fields, $this->token_fields))){
            throw new DomainException("Could not found field '$name'");
        }

        if(array_key_exists($name, $this->access_fields)){
            return $this->getAccessField($name);
        }

        return $this->getTokenField($name);
    }

	public function setCache(Response $response)
	{
		$data = (array)$response->onError(
			function ($response){
				$response->throw();
			}
		)->json();

		$data['expires_at'] = time() + $data['expires_in'] - static::config('token_insurance');
		try {
			$this->saveCache($data);
		} catch (ErrorException $e){
			throw new DomainException($e->getMessage(), $e->getCode());
		}
	}

	public static function config($name)
	{
		return config("glt_auth.$name");
	}

	protected function init()
	{
		$this->initCredentials();
	}

    protected function getAccessField($name)
    {
        if(empty($this->access_fields[$name])){
            throw new DomainException("Field '$name' could not be empty");
        }
        return $this->access_fields[$name];
    }

    protected function getTokenField($name)
    {
        return $this->token_fields[$name];
    }

	abstract protected function saveCache($data);
	abstract protected function loadAuthCache();

	abstract protected function handleRefresh(Response $response);
	abstract protected function handleAuthorize(Response $response);

	abstract protected function initCredentials();

	public function getBaseUrl(): string
	{
		return $this->base_url;
	}

	public function getTokenUrl(): string
	{
		return $this->token_url;
	}

	public function getClientId(): string
	{
		return $this->client_id;
	}

	public function getClientSecret(): string
	{
		return $this->client_secret;
	}

	public function getUsername(): string
	{
		return $this->username;
	}

	public function getPassword(): string
	{
		return $this->password;
	}

	public function getRefreshToken(): string
	{
		return $this->refresh_token;
	}

}