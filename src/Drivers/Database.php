<?php

namespace GLT\LaravelOauth\Drivers;
use GLT\LaravelOauth\Models\OauthCache;
use GLT\LaravelOauth\Models\RestIntegration;
use GLT\LaravelOauth\Services\AuthorizedSendService;
use Illuminate\Http\Client\Response;
use Illuminate\Foundation\Auth\User;

class Database extends AuthCache
{

    protected OauthCache $oauth_cache;
	protected bool $refreshed = false;

    public function __construct(AuthorizedSendService $send_service, User $user)
    {
        if(!config('app.soap_integration_id_name')){
            throw new \DomainException('please setup `app.soap_integration_id_name` - field which you using in users for found integration');
        }
        parent::__construct($send_service, $user);
    }

    public function initAccess(): void
	{
        $this->oauth_cache = OauthCache::prepareUserCache($this->user);

        $this->loadAuthCache();

		if($this->token_type === null){
            $this->handleAuthorize($this->send_service->authorize());
        }
        if($this->expires_at < time()){
            $this->handleRefresh($this->send_service->refresh());
        }
    }


    protected function handleRefresh(Response $response)
    {
        $has_error = false;

        $response->onError(
            function ($response) use (&$has_error){
                $has_error = true;
                $error = $response->json();
                if($error['error'] === 'invalid_request'){
                    $this->handleAuthorize($this->send_service->authorize());
                } else {
                    $response->throw();
                }
            }
        );


        if(!$has_error){
            $this->setCache($response);
        } else {
			$this->refreshed = true;
			$this->oauth_cache->delete();
			$this->handleAuthorize($this->send_service->authorize());

			return;
		}
        $this->loadAuthCache();
    }

    protected function handleAuthorize(Response $response)
    {
        $this->setCache($response);
        $this->loadAuthCache();
    }

    protected function loadAuthCache()
    {
        foreach (array_keys($this->token_fields) as $key){
            $this->token_fields[$key] = $this->oauth_cache->$key;
        }
    }
    protected function saveCache($data)
    {
        foreach (array_keys($this->token_fields) as $key){
            if(!empty($data[$key])){
                $this->oauth_cache->$key = $data[$key];
            }
        }
        $this->oauth_cache->save();

    }

    protected function initCredentials()
    {
        $integration = RestIntegration::find($this->user->{config('app.soap_integration_id_name')});
        if(!$integration){
            throw new \DomainException("Could not found " . RestIntegration::class
                . " by id = '" . $this->user->{config('app.soap_integration_id_name')} . "'", 404);
        }

        foreach (array_keys($this->access_fields) as $key) {
            if(!$integration->$key){
                throw new \DomainException(
                    "'$key' can not be empty, check " . RestIntegration::class ." with id = {$integration->id}",
                    500);
            }
            $this->access_fields[$key] = $integration->$key;
        }
    }
}