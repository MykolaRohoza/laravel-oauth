<?php

namespace GLT\LaravelOauth\Drivers;

use GLT\LaravelOauth\Services\AuthorizedSendService;
use Illuminate\Foundation\Auth\User;

class Factory
{
	public static function getInstance(AuthorizedSendService $send_service, ?User $user = null): iAuthCache
	{
		if(config('glt_auth.driver') && !in_array(config('glt_auth.driver'), ['json', 'database'])){
			throw new \DomainException("Available AUTH_EXT_DRIVER 'json' and 'database'");
		}

		if(strtolower(config('glt_auth.driver')) === 'database'){
			return new Database($send_service, $user);
		}
		return new Json($send_service, $user);
	}
}