<?php

namespace GLT\LaravelOauth\Drivers;
use Illuminate\Http\Client\Response;

class Json extends AuthCache
{

	protected function saveCache($data)
	{
		file_put_contents(static::config('cache_file'), json_encode($data));
	}

	public function initAccess(): void
	{
		if( is_file(static::config('cache_file'))){
			$this->loadAuthCache();
			if($this->token_type === null){
				$this->handleAuthorize($this->send_service->authorize());
			}
			if($this->expires_at < time()){
				$this->handleRefresh($this->send_service->refresh());
			}
		} else {
			$this->handleAuthorize($this->send_service->authorize());
		}
	}


	protected function handleRefresh(Response $response)
	{
		$has_error = false;

		$response->onError(
			function ($response) use (&$has_error){
				$has_error = true;
				$error = $response->json();
				if($error['error'] === 'invalid_request'){
					$this->handleAuthorize($this->send_service->authorize());
				} else {
					$response->throw();
				}
			}
		);


		if(!$has_error){
			$this->setCache($response);
		}
		$this->loadAuthCache();
	}

	protected function handleAuthorize(Response $response)
	{
		$this->setCache($response);
		$this->loadAuthCache();
	}

	protected function loadAuthCache()
	{
		$auth_cache = json_decode(file_get_contents(static::config('cache_file')),true);

        foreach (array_keys($this->token_fields) as $key){
            if(!empty($auth_cache[$key])){
                $this->$key = $auth_cache[$key];
            }
        }
	}

	protected function initCredentials()
	{
        foreach (array_keys($this->access_fields) as $key) {
            $this->$key = static::config($key);
        }
	}
}