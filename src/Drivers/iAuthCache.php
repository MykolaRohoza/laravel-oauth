<?php
namespace GLT\LaravelOauth\Drivers;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Client\Response;


/**
 * @property-read string $base_url
 * @property-read string $token_url
 *
 * @property-read string $client_id
 * @property-read string $client_secret
 * @property-read string $username
 * @property-read string $password
 */

interface iAuthCache
{
	/**
	 * @return string|null
	 */
	public function getAuthorization(): string;

	public function setCache(Response $response);

	public static function config($name);

	/**
	 *
	 */
	public function initAccess(): void;

	public function getBaseUrl(): string;
	public function getTokenUrl(): string;
	public function getClientId(): string;
	public function getClientSecret(): string;
	public function getUsername(): string;
	public function getPassword(): string;
	public function getRefreshToken(): string;

}