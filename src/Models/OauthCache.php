<?php

namespace GLT\LaravelOauth\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

/**
 * @property integer $user_id
 * @property string $token_type
 * @property integer $expires_in
 * @property string $access_token
 * @property string $refresh_token
 * @property integer $expires_at
 *
 * @method save
 */
class OauthCache extends Model
{

    protected $primaryKey = 'user_id';

    public static function prepareUserCache(User $user): static
    {
        $result = static::where('user_id', '=', $user->id)->first();

        if(!$result){
            $result =  new static();
            $result->user_id = $user->id;
        }
        return $result;
    }
}
