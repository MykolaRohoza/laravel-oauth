<?php

namespace GLT\LaravelOauth\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $slug
 * @property string $description
 * @property string $base_url
 * @property string $token_url
 * @property string $username
 * @property string $password
 * @property string $client_id
 * @property string $client_secret
 *
 */

class RestIntegration extends Model
{
    public static string $table_name = 'rest_integrations';

    public function getTable(): string
    {
        return static::$table_name;
    }
	public static function getFillableQuestions(): array
	{
		return [
			'slug' => 'Input unique name for integration',
			'description' => 'Input description',
			'base_url' => 'Base url to api (https://domain.com/)',
			'token_url' => 'Link to oauth token api (oauth/token)',
			'username' => 'Your login (username)',
			'password' => 'Your password',
			'client_id' => 'Your client_id',
			'client_secret' => 'Your client_secret',
		];
	}
}
