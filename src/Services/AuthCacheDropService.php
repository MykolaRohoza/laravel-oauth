<?php

namespace GLT\LaravelOauth\Services;

use GLT\LaravelOauth\Drivers\AuthCache;
use GLT\LaravelOauth\Models\OauthCache;
use Illuminate\Console\Command;

class AuthCacheDropService
{

	protected string $user_class_name;
	protected Command $command;

	public function __construct(string $user_class_name, Command $command)
    {
		$this->user_class_name = $user_class_name;
		$this->command = $command;
    }

    public function clearCache()
    {

		if(AuthCache::config('driver') === 'database'){
			if($user_id = $this->command->argument('user_id')){
				$user = $this->user_class_name::find($user_id);
			} else {
				$user = $this->getUser();
			}
			if($user){
				$count = OauthCache::query()
					->where('user_id', '=', $user->id)
					->delete();
				$this->command->info("$count Oauth Caches were delete for `{$user->name}`");
			}
			$this->command->info("You have not active oauth caches");
		} else {
			$this->dropJsonCache();
		}


	}

	protected function dropJsonCache()
	{
		$filename = AuthCache::config('cache_file');
		unlink($filename);
		$this->command->info("Oauth cache file were delete");
	}

	/**
	 * @return User|null
	 */
	protected function getUser()
	{
		$users = $this->getUsersForSelection();
		if($users){
			$selection = $this->command->choice("Select user to clear cache", array_keys($users));
			return $this->getUserFromSelection($selection, $users);
		}
		return null;
	}


	protected function getUsersForSelection(): array
	{
		$users = [];
		$_users = $this->user_class_name::whereIn('id', OauthCache::all()->pluck('user_id')->toArray())->get();
		foreach ($_users as $_user){
			$show_name = $_user->name . ' - ' . preg_replace('~(^.+\@.{1}).+?(.{1}\..+)~', '$1...$2', $_user->email);
			$users[$show_name] = $_user->id;
		}
		return $users;
	}

	/**
	 * @param string $selection
	 * @param array $users
	 * @return User
	 */
	protected function getUserFromSelection(string $selection, array $users)
	{
		$id = $users[$selection];
		return $this->user_class_name::find($id);
	}

}