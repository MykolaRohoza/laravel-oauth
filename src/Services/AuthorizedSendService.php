<?php
namespace GLT\LaravelOauth\Services;

use Closure;
use ErrorException;
use GLT\LaravelOauth\Drivers\iAuthCache;
use GLT\LaravelOauth\Drivers\Factory;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\RequestException;
use DomainException;
use Illuminate\Foundation\Auth\User;

class AuthorizedSendService
{

	protected PendingRequest $client;
	protected array $log = [];

	protected iAuthCache $auth_cache_service;
	protected bool $is_debug = false;
	protected ?User $user = null;

	public function __construct(?User $user = null)
	{
		$this->user = $user;
	}

	/**
	 * @param iAuthCache $auth_cache_service
	 * @return void
	 */
	public function initializeCustomAccess(iAuthCache $auth_cache_service)
	{
		$this->auth_cache_service = $auth_cache_service;
	}

	/**
	 * @param $uri
	 * @param $resource
	 * @param Closure|null $closure
	 * @return mixed
	 * @throws RequestException|DomainException
	 */
    public function send($uri, $resource, Closure $closure = null)
    {
        $response = $this->getClient()
            ->withHeaders(['Authorization' => $this->auth_cache_service->getAuthorization()])
            ->post($this->calculateFullUrl($uri), $resource->resolve())
            ->onError(
                function ($response){
                    $response->throw();
                }
            )
        ;
        if($closure){
            return $closure($response);
        }
        return $response->json();
    }

	/**
	 * @param $uri
	 * @param $resource
	 * @param Closure|null $closure
	 * @return mixed
	 * @throws RequestException|DomainException
	 */
    public function patch($uri, $resource, Closure $closure = null)
    {
        $response = $this->getClient()
            ->withHeaders(['Authorization' => $this->auth_cache_service->getAuthorization()])
            ->patch($this->calculateFullUrl($uri), $resource->resolve())
            ->onError(
                function ($response){
                    $response->throw();
                }
            )
        ;
        if($closure){
            return $closure($response);
        }
        return $response->json();
    }

	/**
	 * @param $uri
	 * @param $resource
	 * @param Closure|null $closure
	 * @return mixed
	 * @throws RequestException|DomainException
	 */
    public function post($uri, $resource, Closure $closure = null)
    {
        $response = $this->getClient()
            ->withHeaders(['Authorization' => $this->auth_cache_service->getAuthorization()])
            ->post($this->calculateFullUrl($uri), $resource->resolve())
            ->onError(
                function ($response){
                    $response->throw();
                }
            )
        ;
        if($closure){
            return $closure($response);
        }
        return $response->json();
    }

	/**
	 * @param $uri
	 * @param $query
	 * @param Closure|null $closure
	 * @return mixed
	 * @throws RequestException|DomainException
	 */
    public function get($uri, $query = null, Closure $closure = null)
    {
        $response = $this->getClient()
            ->withHeaders(['Authorization' => $this->auth_cache_service->getAuthorization()])
            ->get($this->calculateFullUrl($uri), $query)
            ->onError(
                function ($response){
                    $response->throw();
                }
            )
        ;
        if($closure){
            return $closure($response);
        }
        return $response->json();
    }

	/**
	 * @param $uri
	 * @param $resource
	 * @param Closure|null $closure
	 * @return mixed
	 * @throws RequestException|DomainException
	 */

    public function delete($uri, $resource, Closure $closure = null)
    {
        $response = $this->getClient()
            ->withHeaders(['Authorization' => $this->auth_cache_service->getAuthorization()])
            ->delete($this->calculateFullUrl($uri), $resource->resolve())
            ->onError(
                function ($response){
                    $response->throw();
                }
            )
        ;
        if($closure){
            return $closure($response);
        }
        return $response->json();
    }

	/**
	 * Start to logging urls
	 * @param bool $value
	 * @return void
	 */
	public function setIsDebug(bool $value = true)
	{
		$this->is_debug = $value;
	}

	/**
	 * Get logged urls for current session
	 * @return array
	 */
	public function getLog(): array
	{
		return $this->log;
	}


	protected function getClient()
	{
		if(!isset($this->client)){

			$this->client = Http::asForm()
				->withOptions(['verify' => false])
				->withHeaders([
					'Accept-Encoding' => 'gzip, deflate, br',
					'Accept' => 'application/json',
					'Content-Type' => 'application/json',
				]);

			$this->initAccess();
		}
		return $this->client;

	}

	/**
	 * @return Response
	 * @throws RequestException|DomainException
	 */
	public function authorize(): Response
	{
		return $this->getClient()->post($this->calculateFullUrl($this->auth_cache_service->getTokenUrl()), [
			'grant_type' => 'password',
			'client_id' => $this->auth_cache_service->getClientId(),
			'client_secret' => $this->auth_cache_service->getClientSecret(),
			'username' => $this->auth_cache_service->getUsername(),
			'password' => $this->auth_cache_service->getPassword(),
		]);
	}

	/**
	 * @return Response
	 * @throws RequestException|DomainException
	 */
	public function refresh(): Response
	{

		return $this->getClient()->post($this->calculateFullUrl($this->auth_cache_service->getTokenUrl()) , [
			'grant_type' => 'refresh_token',
			'client_id' => $this->auth_cache_service->getClientId(),
			'client_secret' => $this->auth_cache_service->getClientSecret(),
			'refresh_token' => $this->auth_cache_service->getRefreshToken(),
			'scope' => '',
		]);

	}

	protected function initAccess()
	{
		if(!isset($this->auth_cache_service)){
			$this->auth_cache_service = Factory::getInstance($this, $this->user);
			$this->auth_cache_service->initAccess();
		}
	}

	protected function calculateFullUrl($uri): string
	{
		$full_url = $this->auth_cache_service->getBaseUrl() . $uri;
		if($this->is_debug){
			$this->log[] = $full_url;
		}
		return $full_url;
	}

}